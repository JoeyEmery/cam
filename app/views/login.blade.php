@extends('layouts.base')

@section('content')
    @include('common/navbar', ['container_width' => '780px'])

    <div class="container" style="margin-top: 50px; padding-bottom: 50px;">
        <div class="row">
            <div class="col-md-4 col-md-offset-2">
                <img src="https://onlyfans.com/theme/onlyfans/images/phones.png" class="img-responsive" />
            </div>
            <div class="col-md-4">
                <div class="login-box">
                    <div class="logo">
                        <img src="http://placehold.it/500x100/333/fff?text=Logo goes here" class="img-responsive" />
                    </div>
                    <a href="#" class="btn btn-primary btn-facebook"><span class="fa fa-facebook"></span> Login with Facebook</a>
                    <hr />
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-login">
                            <p>Use the form below if you wish to login or register without Facebook, please note some features will not be available if you choose this option.</p>
                            <form action="{{ route('home.login.post') }}" method="post">
                                <div class="form-group">
                                    <input type="text" name="email" class="form-control" placeholder="Email" />
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" class="form-control" placeholder="Password" />
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary" value="Login" />
                                </div>
                                <div class="form-group" style="margin: 0;">
                                    <a href="#tab-register" class="btn btn-default" data-toggle="tab">Create new account</a>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane" id="tab-register">
                            <form action="{{ route('home.register.post') }}" method="post">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="username" placeholder="Username" />
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="email" placeholder="Email" />
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" name="password" placeholder="Password" />
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="name" placeholder="Full name" />
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary" value="Register" />
                                </div>
                                <div class="form-group" style="margin: 0;">
                                    <a href="#tab-login" class="btn btn-default" data-toggle="tab">Log into existing account</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modals')
    <div class="modal fade" tabindex="-1" role="dialog" id="modal-register">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <div class="icon">
                        <span class="fa fa-user-secret"></span>
                    </div>
                    <h4 class="modal-title">Register a new account</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                </div>
                <div class="modal-body">
                    <form action="/register" method="post">
                        <div class="form-group">
                            <input type="text" class="form-control" name="username" placeholder="Username" />
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="email" placeholder="Email" />
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="password" placeholder="Password" />
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="name" placeholder="Full name" />
                        </div>
                        <div class="form-group" style="margin: 0;">
                            <input type="submit" class="btn btn-primary" value="Create account" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
