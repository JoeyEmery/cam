<div class="feed-item">
    <div class="row">
        @if(isset($picture) && $picture == false)
        @else
            <div class="col-md-1 image">
                <img src="http://lorempixel.com/100/100/people" class="img-responsive" />
            </div>
        @endif
        <div @if(isset($picture) && $picture == false) class="col-md-12 post" @else class="col-md-11 post" @endif >
            <h3><a href="{{ route('user.profile', $post->user->username) }}">{{ $post->user->name }} <small>{{ '@' . $post->user->username }}</small></a></h3>

            @if($post->content)
                <p>{{ $post->content }}</p>
            @endif

            @if($post->post_media)
                <div @if(count($post->post_media) == 1) class="media media-single" @else class="media media-multiple clearfix" @endif >
                    @foreach($post->post_media as $media)
                        @if($media->media->type == 'image')
                            <div class="media-content">
                                <a href="#"><img src="{{ $media->media->path }}" /></a>
                            </div>
                        @elseif($media->media->type == 'video')

                        @endif
                    @endforeach

                </div>
            @endif

            <ul class="meta clearfix">
                <li><a href="#">Like (2,135 likes)</a></li>
                <li><a href="#">Reply</a></li>
                <li>2 minutes ago</li>
            </ul>
        </div>
    </div>
</div>
