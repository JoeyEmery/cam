<nav class="navbar navbar-default">
    <div class="container-fluid" @if(isset($container_width)) style="width: {{ $container_width }}" @endif>
        <div class="row">
            <div class="col-md-12">
                @if(Auth::check())
                    @include('common/menu-user')
                @else
                    @include('common/menu-guest')
                @endif
            </div>
        </div>
    </div>
</nav>
