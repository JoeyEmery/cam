<ul class="nav">
    <li>
        <a href="#">
            <span class="fa fa-question"></span> How it works
        </a>
    </li>
    <li>
        <a href="#">
            <span class="fa fa-align-justify"></span> FAQs
        </a>
    </li>
    <li>
        <a href="#">
            <span class="fa fa-address-book-o"></span> Contact
        </a>
    </li>
    <li>
        <a href="#">
            <span class="fa fa-list-alt"></span> Terms
        </a>
    </li>
    <li>
        <a href="#">
            <span class="fa fa-list-alt"></span> Privacy
        </a>
    </li>
</ul>
