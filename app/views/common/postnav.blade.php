<div class="post-navbar">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 current-user">
                <div class="row">
                    <div class="col-md-3">
                        <img src="http://lorempixel.com/100/100/people" class="img-responsive" />
                    </div>
                    <div class="col-md-9">
                        <h2>{{ $user->name }}</h2>
                        <p>{{ '@' . $user->username }}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 stats">
                <ul>
                    <li>
                        <h3>{{ count($user->subscribers) }}</h3>
                        <p>Subscribers</p>
                    </li>
                    <li>
                        <h3>{{ count($user->posts) }}</h3>
                        <p>Posts</p>
                    </li>
                    <li>
                        <h3>{{ count($images) }}</h3>
                        <p>Images</p>
                    </li>
                    <li>
                        <h3>14</h3>
                        <p>Videos</p>
                    </li>
                    <li>
                        <h3>3</h3>
                        <p>Livestreams</p>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 subscribe">
                @if(!$subscribed)
                    <a @if($user->subscription_price == 0) href="/subscribe/{{ $user->id }}" @else href="#" data-toggle="modal" data-target="#modal-subscribe" @endif class="btn btn-primary">Subscribe</a>
                @endif
            </div>
        </div>
    </div>
</div>
