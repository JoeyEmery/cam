<ul class="nav">
    <li @if(Route::currentRouteName() == 'feed.recent') class="active" @endif>
        <a href="{{ route('feed.recent') }}">
            <span class="fa fa-list-alt"></span> Feed
        </a>
    </li>
    <li @if(Route::currentRouteName() == 'user.profile') class="active" @endif>
        <a href="{{ route('user.profile', Auth::user()->username) }}">
            <span class="fa fa-user-circle-o"></span> Profile
        </a>
    </li>
    {{-- <li @if(Route::currentRouteName() == 'user.fans') class="active" @endif>
        <a href="#">
            <span class="fa fa-address-book-o"></span> Fans
        </a>
    </li> --}}
    <li @if(Route::currentRouteName() == 'user.settings') class="active" @endif>
        <a href="{{ route('user.settings') }}">
            <span class="fa fa-cog"></span> Settings
        </a>
    </li>
    <li class="pull-right">
        <a href="/logout">
            <span class="fa fa-power-off"></span> Logout
        </a>
    </li>
</ul>
