@extends('layouts.base')

@section('content')
    @include('common/navbar')
    {{-- @include('common/postnav') --}}

    <div class="feed">
        <div class="heading-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3">
                        <div class="heading" style="padding-left: 0;">
                            <h2>New Post <span class="fa fa-pencil-square-o pull-right"></span></h2>
                        </div>
                    </div>
                    <div class="col-md-6 border-left border-right">
                        <div class="heading">
                            <h2>Recent Activity <span class="fa fa-list-alt pull-right"></span></h2>
                        </div>
                    </div>
                    <div class="col-md-3" style="padding-left: 0;">
                        <div class="heading" style="padding-right: 0;">
                            <h2>User Information <span class="fa fa-user-circle-o pull-right"></span></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3 new-post">
                    <form action="/post" method="post">
                        <div class="form-group">
                            <div class="post">
                                <div class="user">
                                    <div class="row">
                                        <div class="col-md-2" style="padding-right: 0;">
                                            <img src="http://lorempixel.com/100/100/people" class="img-responsive" />
                                        </div>
                                        <div class="col-md-10">
                                            <h4>{{ Auth::user()->name }}</h4>
                                            <p>{{ '@' . Auth::user()->username }}</p>
                                        </div>
                                    </div>
                                </div>
                                <textarea name="content" rows="4" class="form-control" placeholder="Say something..."></textarea>
                                <div class="dropzone-container clearfix"></div>
                                <div class="footer">
                                    <div class="row">
                                        <div class="col-md-7">
                                            <ul>
                                                <li><a href="#" class="upload-image"><span class="fa fa-camera"></span></a></li>
                                                <li><a href="#"><span class="fa fa-video-camera"></span></a></li>
                                            </ul>
                                        </div>
                                        <div class="col-md-5">
                                            <input type="submit" class="btn btn-primary" value="Post" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                    <div class="go-live">
                        <a href="#" class="btn btn-primary"><span class="fa fa-bullhorn"></span> Go Live</a>
                        <p>Clicking the button above will bring up the streaming dialog which will allow you to start livestreaming.</p>
                    </div>
                </div>
                <div class="col-md-6 content">
                    @if(!$posts->isEmpty())
                        @foreach($posts as $post)
                            @include('listing/post')
                        @endforeach
                    @else
                        <div class="subscription-required push-top-double">
                            <span class="icon fa fa-pencil-square-o"></span>
                            <h3>Your feed is empty.</h3>
                            <p style="margin-left: auto; margin-right: auto; width: 60%;">This could be because you either have no active subscriptions, or the people you are subscribing to are yet to have posted.</p>
                        </div>
                    @endif
                </div>
                <div class="col-md-3 details">
                    {{-- <div class="panel panel-default" style="margin-top: 15px;">
                        <div class="panel-heading">
                            <strong>Biography</strong>
                        </div>
                        <div class="panel-body">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        </div>
                    </div>

                    <div class="panel panel-default" style="margin-top: 15px;">
                        <div class="panel-heading">
                            <strong>Social Media</strong>
                        </div>
                        <div class="panel-body">
                            <div class="list-group" style="margin: 0;">
                                <a href="#" class="list-group-item"><span class="fa fa-twitter" style="margin-right: 5px;"></span> Twitter</a>
                                <a href="#" class="list-group-item"><span class="fa fa-facebook" style="margin-right: 5px;"></span> Facebook</a>
                                <a href="#" class="list-group-item"><span class="fa fa-instagram" style="margin-right: 5px;"></span> Instagram</a>
                                <a href="#" class="list-group-item"><span class="fa fa-tumblr" style="margin-right: 5px;"></span> Tumblr</a>
                            </div>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript" src="/assets/js/vendor/dropzone.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.upload-image').on('click', function(e) {
                e.preventDefault();

                $('.dropzone-container').dropzone({
                    url: '/post/upload/image',
                    maxFiles: 5,
                    acceptedFiles: '.png, .jpg, .jpeg, .gif',
                    addRemoveLinks: true,
                    init: function() {
                        // Catch errors.
                        this.on('error', function(message) {
                            $(message.previewElement).remove();
                            alert('Please make sure you only upload images.');
                        });

                        // Added file.
                        this.on('addedfile', function(file) {
                            $('.new-post form input[type=submit]').attr('disabled', 'disabled');
                        });

                        // Remove file
                        this.on('removedfile', function(file) {
                            console.log(file);
                        });

                        this.on('success', function(file, response) {
                            $(file.previewElement).append('<input type="hidden" name="media[]" value="' + response.id +'" />');
                        });

                        // Uploads complete.
                        this.on('queuecomplete', function(complete) {
                            $('.new-post form input[type=submit]').removeAttr('disabled');
                        });
                    }
                });

                $('.dropzone-container.dz-clickable').click();
            });
        });
    </script>
@endsection
