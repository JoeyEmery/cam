@extends('layouts.base')

@section('content')
    @include('common/navbar')

    <div class="feed">
        <div class="heading-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-offset-3 col-md-6 border-left border-right" style="padding-left: 0; padding-right: 0;">
                        <div class="heading">
                            <h2>Edit Settings <span class="fa fa-cogs pull-right"></span></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 col-md-offset-3" style="padding-left: 0; padding-right: 0;">
                    <div class="content" style="padding: 30px;">
                        <form action="{{ route('user.settings.update') }}" method="post">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" name="name" class="form-control" placeholder="Name" value="{{ Auth::user()->name }}" />
                            </div>
                            <div class="form-group">
                                <label for="biography">Biography</label>
                                <textarea style="resize: none;" rows="5" name="biography" class="form-control" placeholder="Biography">{{ Auth::user()->biography }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="name">Subscription Price (&pound;)</label>
                                <div class="input-group">
                                    <span class="input-group-addon">&pound;</span>
                                    <input type="text" name="subscription_price" class="form-control" placeholder="Subscription Price" value="{{ number_format(Auth::user()->subscription_price, 2) }}" />
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary" value="Save changes" />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
