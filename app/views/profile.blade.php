@extends('layouts.base')

@section('content')
    @include('common/navbar')
    @include('common/postnav')

    <div class="feed">
        <div class="heading-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3">
                        <div class="heading borderless" style="padding-left: 0;">
                            <h2>Media <span class="fa fa-picture-o pull-right"></span></h2>
                        </div>
                    </div>
                    <div class="col-md-6 border-left border-right" style="padding-left: 0; padding-right: 0;">
                        <div class="heading">
                            <h2>Recent Activity <span class="fa fa-list-alt pull-right"></span></h2>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="heading borderless" style="padding-right: 0;">
                            <h2>User Information <span class="fa fa-user-circle-o pull-right"></span></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3 media">
                    @if($subscribed || $user->subscription_price == 0)
                        @if($images)
                            <div class="panel panel-default" style="margin-top: 15px;">
                                <div class="panel-heading">
                                    <strong>Images</strong> ({{ count($images) }})
                                </div>
                                <div class="panel-body">
                                    <ul class="images">
                                        @foreach($images as $key => $image)
                                            <li @if($key % 2 == 0) style="margin-left: 0;" @endif><img src="{{ $image->path }}" class="img-responsive" /></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        @endif

                        <div class="panel panel-default" style="margin-top: 15px;">
                            <div class="panel-heading">
                                <strong>Videos</strong> (14)
                            </div>
                            <div class="panel-body">
                                <ul class="images">
                                    @for($i=0;$i<2;$i++)
                                        <li style="margin-left: 0;"><img src="http://lorempixel.com/200/200/people" class="img-responsive" /></li>
                                        <li><img src="http://lorempixel.com/200/200/people" class="img-responsive" /></li>
                                    @endfor
                                </ul>
                            </div>
                        </div>
                    @else
                        <div class="subscription-required push-top">
                            <span class="icon fa fa-lock"></span>
                            <h3>Subscription Required</h3>
                            <p>Before you can see this users media you must have an active subscription.</p>
                            <a @if($user->subscription_price == 0) href="/subscribe/{{ $user->id }}" @else href="#" data-toggle="modal" data-target="#modal-subscribe" @endif class="btn btn-primary"><span class="fa fa-credit-card"></span> <strong>Subscribe</strong> (&pound;{{ $user->subscription_price }}/pm)</a>
                        </div>
                    @endif
                </div>
                <div class="col-md-6" style="padding-left: 0; padding-right: 0;">
                    <div class="content">
                        @if($subscribed || $user->subscription_price == 0)
                            @if(!$user->posts->isEmpty())
                                @foreach($user->posts as $post)
                                    @include('listing.post', ['picture' => false])
                                @endforeach
                            @else
                                <div class="subscription-required push-top-double">
                                    <span class="icon fa fa-pencil-square-o"></span>
                                    <h3>Nothing to show yet.</h3>
                                    <p>This user has yet to write a post.</p>
                                </div>
                            @endif
                        @else
                            <div class="subscription-required push-top">
                                <span class="icon fa fa-lock"></span>
                                <h3>Subscription Required</h3>
                                <p>This user has their posts hidden from anybody who isn't a subscriber.</p>
                                <a @if($user->subscription_price == 0) href="/subscribe/{{ $user->id }}" @else href="#" data-toggle="modal" data-target="#modal-subscribe" @endif class="btn btn-primary"><span class="fa fa-credit-card"></span> <strong>Subscribe</strong> (&pound;{{ $user->subscription_price }}/pm)</a>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col-md-3 details">
                    @if($subscribed || $user->subscription_price == 0)
                        @if(!empty($user->biography))
                            <div class="panel panel-default" style="margin-top: 15px;">
                                <div class="panel-heading">
                                    <strong>Biography</strong>
                                </div>
                                <div class="panel-body">
                                    <p>{{ $user->biography }}</p>
                                </div>
                            </div>
                        @endif

                        @if($user->links)
                            <div class="panel panel-default" style="margin-top: 15px;">
                                <div class="panel-heading">
                                    <strong>Social Media</strong>
                                </div>
                                <div class="panel-body">
                                    <div class="list-group user-links" style="margin: 0;">
                                        @foreach($user->links as $link)
                                            <a href="{{ $link->link }}" target="_blank" rel="nofollow" class="list-group-item"><span class="fa fa-{{ $link->type }}" style="margin-right: 5px;"></span> {{ $link->type }}</a>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @endif
                    @else
                        <div class="subscription-required push-top">
                            <span class="icon fa fa-lock"></span>
                            <h3>Subscription Required</h3>
                            <p>Before you can view this users information you need to subscribe.</p>
                            <a @if($user->subscription_price == 0) href="/subscribe/{{ $user->id }}" @else href="#" data-toggle="modal" data-target="#modal-subscribe" @endif class="btn btn-primary"><span class="fa fa-credit-card"></span> <strong>Subscribe</strong> (&pound;{{ $user->subscription_price }}/pm)</a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modals')
    <div class="modal fade" tabindex="-1" role="dialog" id="modal-subscribe">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <div class="icon">
                        <span class="fa fa-credit-card"></span>
                    </div>
                    <h4 class="modal-title">Create a new subscription</h4>
                    <p>This user requires a monthly payment of &pound;{{ $user->subscription_price }} to subscribe, please enter your card details below.</p>
                </div>
                <div class="modal-body">
                    <form action="/subscribe/payment/{{ $user->id }}" method="post" id="form-subscribe">
                        <div class="form-group">
                            <input type="text" class="form-control" name="card_number" placeholder="Card Number" value="4242424242424242" />
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="postcode" placeholder="Postcode" value="ne259na" />
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="number" class="form-control" name="card_expiry_month" placeholder="Expiration Month" value="03" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="number" class="form-control" name="card_expiry_year" placeholder="Expiration Year" value="18" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="number" class="form-control" name="card_cvc" placeholder="CVC" value="111" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group" style="margin: 0;">
                            <input type="submit" class="btn btn-primary" value="Subscribe" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#form-subscribe').on('submit', function(e) {
                e.preventDefault();

                Stripe.setPublishableKey('pk_test_jKv8o05YZCxtannZIvZJ8bs4');
                Stripe.card.createToken({
                    number: $('input[name=card_number]').val(),
                    cvc: $('input[name=card_cvc]').val(),
                    exp_month: $('input[name=card_expiry_month]').val(),
                    exp_year: $('input[name=card_expiry_year]').val(),
                    address_zip: $('input[name=postcode]').val()
                }, function(status, response) {
                    switch(status) {
                        case 200:
                            window.location = '/subscribe/{{ $user->id }}/payment?token=' + response.id;
                        break;
                    }
                });
            });
        });
    </script>
@endsection
