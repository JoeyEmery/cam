<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="/assets/css/app.css" rel="stylesheet" type="text/css">
        <title>CamSite Baby</title>
    </head>
    <body style="background: #f9f9f9;">
        @yield('content')

        @yield('modals')

        <script type="text/javascript" src="/assets/js/vendor/jquery.js"></script>
        <script type="text/javascript" src="/assets/js/vendor/bootstrap.js"></script>
        @yield('javascript')
    </body>
</html>
