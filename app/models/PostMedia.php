<?php

class PostMedia extends Eloquent {

    protected $table = 'post_media';

    /* Relationships */
    public function media()
    {
        return $this->hasOne('Media', 'id', 'media_id');
    }

}
