<?php

class Subscription extends Eloquent {

    protected $table = 'subscriptions';

    public static function isSubscribed($who, $to)
    {
        $subscription = Subscription::where('subscriber_id', '=', $who)->where('subscribee_id', '=', $to)->first();
        return ($who == $to) ? true : ($subscription) ? true : false;
    }

}
