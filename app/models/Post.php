<?php

class Post extends Eloquent {

    protected $table = 'posts';

    /* Relationships */
    public function user()
    {
        return $this->hasOne('User', 'id', 'user_id');
    }

    public function post_media()
    {
        return $this->hasMany('PostMedia', 'post_id', 'id');
    }

}
