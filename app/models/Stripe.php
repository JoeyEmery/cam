<?php

class Stripe {
    public static function setKey()
    {
        \Stripe\Stripe::setApiKey("sk_test_rJUP0Ze0FHBmENMSfbYWrFVv");
    }

    public static function createCustomer($name)
    {
        self::setKey();

        $customer = \Stripe\Customer::create([
            'description' => 'Stripe customer for ' . $name
        ]);

        return $customer;
    }

    public static function createSubscription($customer, $token, $amount)
    {
        self::setKey();

        $subscription = \Stripe\Subscription::create([
            'customer' => $customer,
            'source' => $token,
            'plan' => 'basic',
            'quantity' => $amount
        ]);

        return $subscription;
    }
}
