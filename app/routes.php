<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Guest User.
Route::group(['before' => 'guest'], function() {
    // User
    Route::get('/login', ['uses' => 'HomeController@getLogin', 'as' => 'home.login']);
    Route::get('/register', ['uses' => 'HomeController@getRegister', 'as' => 'home.register']);
    Route::post('/login', ['uses' => 'HomeController@postLogin', 'as' => 'home.login.post']);
    Route::post('/register', ['uses' => 'HomeController@postRegister', 'as' => 'home.register.post']);

    // Profile
    Route::get('/user/{id}', ['uses' => 'UserController@getProfile', 'as' => 'user.profile']);
});

// Authenticated user.
Route::group(['before' => 'auth'], function() {

    // Feed
    Route::get('/', ['uses' => 'FeedController@getRecent', 'as' => 'feed.recent']);
    Route::post('/post', ['uses' => 'FeedController@postNewPost', 'as' => 'feed.new.post']);
    Route::post('/post/upload/{type}', ['uses' => 'FeedController@postNewPostUpload', 'as' => 'feed.new.post.upload']);

    // Subscribe
    Route::get('/subscribe/{id}', ['uses' => 'UserController@getSubscribe', 'as' => 'user.subscribe']);
    Route::get('/subscribe/{id}/payment', ['uses' => 'UserController@getSubscribePayment', 'as' => 'user.subscribe.payment']);

    // Settings.
    Route::get('/settings', ['uses' => 'UserController@getSettings', 'as' => 'user.settings']);
    Route::post('/settings', ['uses' => 'UserController@postSettings', 'as' => 'user.settings.update']);

    // User/Profile
    Route::get('/logout', ['uses' => 'UserController@getLogout', 'as' => 'user.logout']);
    Route::get('/user/{id}', ['uses' => 'UserController@getProfile', 'as' => 'user.profile']);
});
