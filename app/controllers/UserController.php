<?php

class UserController extends Controller {

    public function getProfile($username)
    {
        // Get the user.
        $user = User::with('posts.post_media.media', 'links', 'subscribers')->where('username', '=', $username)->first();

        // No user? 404.
        if(!$user) {
            return Redirect::to('/notfound');
        }

        // Get the users images.
        $images = Media::where('user_id', '=', Auth::user()->id)->where('type', '=', 'image')->get();

        // Render the view.
        return View::make('profile', [
            'user' => $user,
            'images' => $images,
            'subscribed' => Subscription::isSubscribed(Auth::user()->id, $user->id),
        ]);
    }

    public function getLogout()
    {
        Auth::logout();
        return Redirect::to('/');
    }

    public function getSubscribe($id)
    {
        $user = User::find($id);

        if($user->subscription_price == 0) {
            $subscription = new Subscription;
            $subscription->subscriber_id = Auth::user()->id;
            $subscription->subscribee_id = $user->id;
            $subscription->save();
        }

        return Redirect::to(route('user.profile', $user->username));
    }

    public function getSubscribePayment($id) {
        $user = User::find($id);

        // Create the Stripe subscription.
        $token = Input::get('token');
        $subscription = Stripe::createSubscription(Auth::user()->stripe_id, $token, 1000);

        // Create the db subscription.
        if($subscription->id) {
            $subscribe = new Subscription;
            $subscribe->subscriber_id = Auth::user()->id;
            $subscribe->subscribee_id = $id;
            $subscribe->expires = $subscription->current_period_end;

            $subscribe->save();

            return Redirect::to(route('user.profile', $user->username));
        }
    }

    public function getSettings()
    {
        return View::make('settings');
    }

    public function postSettings()
    {
        Auth::user()->name = Input::get('name', Auth::user()->name);
        Auth::user()->biography = Input::get('biography', Auth::user()->biography);
        Auth::user()->subscription_price = Input::get('subscription_price', Auth::user()->subscription_price);
        Auth::user()->save();

        return Redirect::to(route('user.settings'));
    }

}
