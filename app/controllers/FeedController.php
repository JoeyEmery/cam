<?php

class FeedController extends Controller {

    public function getRecent()
    {
        $the_subscribers = Auth::user()->subscribers;
        $end_subscribers = [Auth::user()->id];
        foreach($the_subscribers as $sub) {
            $end_subscribers[] = $sub->subscriber_id;
        }

        $posts = Post::whereIn('user_id', $end_subscribers)->with('post_media.media', 'user')->orderBy('created_at', 'desc')->get();

        return View::make('dashboard', [
            'posts' => $posts
        ]);
    }

    public function postNewPost()
    {
        $post = new Post;
        $post->user_id = Auth::user()->id;
        $post->content = Input::get('content');
        $post->save();

        if(Input::has('media')) {
            foreach(Input::get('media') as $key => $media_id) {
                $postmedia = new PostMedia;
                $postmedia->post_id = $post->id;
                $postmedia->media_id = $media_id;
                $postmedia->save();
            }
        }

        return Redirect::to(route('feed.recent'));
    }

    public function postNewPostUpload($type)
    {
        if(Input::hasFile('file')) {
            // Upload the file into the public/upload/images directory.
            $file = Input::file('file');
            $path = '/uploads/' . $type . 's/';
            $filename = md5(time() . Auth::user()->id) . '.' . $file->getClientOriginalExtension();
            $file->move(public_path() . $path, $filename);

            // Save the media into the database.
            $media = new Media;
            $media->user_id = Auth::user()->id;
            $media->type = $type;
            $media->path = $path . $filename;
            $media->format = $file->getClientOriginalExtension();
            $media->save();

            return Response::json([
                'success' => true,
                'id' => $media->id
            ], 200);
        }

        return Response::json('error', 500);
    }

}
