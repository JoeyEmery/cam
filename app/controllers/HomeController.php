<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function getLogin()
	{
		return View::make('login');
	}

    public function getRegister()
    {
        return View::make('login', [
            'register' => true
        ]);
    }

    public function postLogin()
    {
        if(Auth::attempt([
            'email' => Input::get('email'),
            'password' => Input::get('password')
        ])) {
            return Redirect::to('/');
        } else {
            return 'Account not found';
        }
    }

    public function postRegister()
    {
        $validator = Validator::make(Input::all(), [
            'name' => 'required|min:5',
            'password' => 'required',
            'email' => 'required|unique:users',
            'username' => 'required|unique:users',
        ]);

        if($validator->passes()) {
            // Create the user.
            $user = new User;
            $user->name = Input::get('name');
            $user->password = Hash::make(Input::get('password'));
            $user->email = Input::get('email');
            $user->username = Input::get('username');

            // Create a stripe customer.
            $customer = Stripe::createCustomer($user->name);
            $user->stripe_id = $customer->id;

            $user->save();

            Auth::login($user);
            return Redirect::to('/');
        }

        return $validator->messages();
    }

}
